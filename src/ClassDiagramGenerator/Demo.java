package ClassDiagramGenerator;
import japa.parser.JavaParser;
import japa.parser.ast.CompilationUnit;
import japa.parser.ast.body.FieldDeclaration;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.body.VariableDeclarator;
import japa.parser.ast.body.VariableDeclaratorId;
import japa.parser.ast.visitor.*;

import java.io.FileInputStream;
import java.util.Scanner;



public class Demo {
	
	public static void main(String[] args){
		pathFetcher connor = new pathFetcher();
		connor.setProjPath();
		
		projectExplorer pe = new projectExplorer(connor.getProjPath());
		
		System.out.println("Project Information Obtained");
		
		connor.setMainPath();
		
		System.out.println("Main class loaded, class recursing starting...");
		
		//pe.startRecursing(connor.getMainPath());
		pe.generateDiagramFiles();
		
	}

}
