package ClassDiagramGenerator;
import japa.parser.JavaParser;
import japa.parser.ParseException;
import japa.parser.ast.CompilationUnit;
import japa.parser.ast.TypeParameter;
import japa.parser.ast.body.BodyDeclaration;
import japa.parser.ast.body.ClassOrInterfaceDeclaration;
import japa.parser.ast.body.ConstructorDeclaration;
import japa.parser.ast.body.FieldDeclaration;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.body.VariableDeclarator;
import japa.parser.ast.body.VariableDeclaratorId;
import japa.parser.ast.expr.MethodCallExpr;
import japa.parser.ast.expr.ObjectCreationExpr;
import japa.parser.ast.type.ClassOrInterfaceType;
import japa.parser.ast.visitor.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;


public class classHandler {
	
	private File ifile;
	private static File ofile;
	private ArrayList<String> vars = new ArrayList<String>();
	private static FileWriter writer;
	
	public classHandler(File f){
		ifile = f;
	}
	
	public void gatherClassData(){
		methodPrinter(ifile);
	}
	
	private void methodPrinter(File f){
		try {
			FileInputStream in = new FileInputStream(f);
	
		    CompilationUnit cu;
		    try {
		        // parse the file
		        cu = JavaParser.parse(in);
		    } finally {
		    	in.close();
		    }
		    
		    //create ofile using className
		    new classVisitor().visit(cu, null);
		    	
		    // visit and print the methods names
		    new MethodVisitor().visit(cu, null);
		   		    
		} catch (Exception e){
			System.out.println("Error " + e.getMessage());
		}
		
	}
	
	private static void writeToFile(String s, boolean exists){
		try {
			if(exists){
				writer = new FileWriter(ofile, true);
			} else {
				writer = new FileWriter(ofile, false);
			}
			writer.write(s);
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
///home/connor/git/cs409-analyser/src/ClassDiagramGenerator/Demo.java
	/**
	 * Simple visitor implementation for visiting MethodDeclaration nodes. 
	 */
	private static class MethodVisitor extends VoidVisitorAdapter {

	    @Override
	    public void visit(MethodDeclaration n, Object arg) {
	        // here you can access the attributes of the method.
	        // this method will be called for all methods in this 
	        // CompilationUnit, including inner class methods
	    	if(n.getParameters()==null){
	    		System.out.println(n.getName() + " (" +  n.getType()+ ") ");
	    		writeToFile(n.getName() + " (" +  n.getType()+ ") \n", true);
	    	} else{
	    		//System.out.println(n.getName() + " " +  n.getParameters() + " (" +  n.getType()+ ") ");
	    		writeToFile(n.getName() + " " +  n.getParameters() + " (" +  n.getType()+ ") \n", true);
	    	}
	    }
        @Override
        public void visit(FieldDeclaration n, Object arg) {
            for (VariableDeclarator var : n.getVariables()) {
                var.accept(this, arg);
                
                //System.out.println(n.getType());
                writeToFile(n.getType().toString() + "\n", true);
               }
        }
        @Override
        public void visit(VariableDeclarator n, Object arg) {
            n.getId().accept(this, arg);

        }
        @Override
        public void visit(VariableDeclaratorId n, Object arg) {
            //System.out.print(n.getName() + " : " );
            writeToFile(n.getName() + " : " , true);
        }
        @Override
        public void visit(ConstructorDeclaration n, Object arg){
        	
        }
	}
	private static class classVisitor extends VoidVisitorAdapter {
        @Override
        public void visit(ClassOrInterfaceDeclaration n, Object arg){
        	
        	System.out.println(n.getName());
        	ofile = new File(n.getName() + ".txt");
        	
        	
        	List<ClassOrInterfaceType> exs = n.getExtends();
        	List<ClassOrInterfaceType> ins = n.getImplements();
        	List<TypeParameter> pars = n.getTypeParameters();
        	
        	String exstr;
        	String instr;
        	String parstr;
        	
        	if(exs == null){exstr = "";} else{ exstr = " extends " + n.getExtends().toString(); }
        	if(ins == null){instr = "";} else{ instr = " implements " + n.getImplements().toString(); }
        	if(pars == null){parstr = "";} else{ parstr = n.getTypeParameters().toString(); }
        	

        	String s = (n.getName()
        			+ parstr
        			+ exstr
        			+ instr
        			+"\n");
        	
        	writeToFile(s, false);

        	
        }
	}
	
}

