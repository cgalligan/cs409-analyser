package ClassDiagramGenerator;
import java.io.File;
import java.util.ArrayList;

public class projectExplorer {
	
	String[] packageList;
	ArrayList<String> classList;
	ArrayList<File> classPathList;
	String srcpath;
	
	public projectExplorer(String dir){
		srcpath = dir;
		packageList = new File(srcpath).list();
		//printPackages();
		
		setclassPathList();
		//printClassPathList();
		//printClassList();
	}
	
	private void setclassPathList(){
		classPathList = new ArrayList<File>();
		classList = new ArrayList<String>();
		
		for(int i = 0; i < packageList.length; i++){
			
			String cp = srcpath + "/" + packageList[i];;
			
			File[] fs = new File(cp).listFiles();
			
			for(int j = 0; j < fs.length; j++){
				classList.add(addToClassList(fs[j].getName()));
				classPathList.add(fs[j]);
			}
		}
	}
	
	public void startRecursing(String mp){
		File f = new File(mp);
		classHandler ch = new classHandler(f);
		ch.gatherClassData();
	}
	
	//To get rid of file extension
	private String addToClassList(String s){
		int pos = s.lastIndexOf(".");
		if (pos > 0) {
		    s = s.substring(0, pos);
		}
		return s;
	}
	
	private void printPackages(){
		for(int i = 0; i < packageList.length; i++){
			System.out.println(packageList[i]);
		}
	}
	
	private void printClassPathList(){
		for(int i = 0; i < classPathList.size(); i++){
			System.out.println(classPathList.get(i));
		}		
	}
	
	private void printClassList(){
		for(int i = 0; i < classList.size(); i++){
			System.out.println(classList.get(i));
		}
	}
	
	public ArrayList<File> getClasses(){
		return classPathList;
	}
	
	public void generateDiagramFiles(){
		for(File f : classPathList){
			classHandler ch = new classHandler(f);
			ch.gatherClassData();
		}
	}

}
