package ClassDiagramGenerator;
import java.util.Scanner;


public class pathFetcher {
	
	String projPath;
	String mainPath;
	
	public pathFetcher(){
		
	}
	
	public void setProjPath(){
		System.out.println("Enter the path to the src folder of the project to be analysed");
		projPath = getInput();
	}
	
	public void setMainPath(){
		System.out.println("Enter the path to the class containing main");
		mainPath = getInput();
	}
	
	private String getInput(){
		
		Scanner sc = new Scanner(System.in);
		String s = sc.next();
		
		if(pathValidate(s)){
			return s;
		}
		else{
			System.out.println("Try Again");
			getInput();
		}
		return null;
	}
	
	private boolean pathValidate(String st){
		return true;
	}
	
	public String getProjPath(){
		return projPath;
	}
	
	public String getMainPath(){
		return mainPath;
	}

}
